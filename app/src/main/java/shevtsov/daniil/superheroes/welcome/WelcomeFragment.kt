package shevtsov.daniil.superheroes.welcome

import android.os.Bundle
import android.view.View
import androidx.compose.material.MaterialTheme
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import shevtsov.daniil.superheroes.R
import shevtsov.daniil.superheroes.core.util.viewLifecycleLazy
import shevtsov.daniil.superheroes.databinding.FragmentWelcomeBinding

class WelcomeFragment : Fragment(R.layout.fragment_welcome) {

    private val binding by viewLifecycleLazy { FragmentWelcomeBinding.bind(requireView()) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.apply {
            composeView.setContent {
                MaterialTheme {
                    WelcomeScreen(navigateToCharactersAction = {
                        findNavController().navigate(R.id.character_list_fragment)
                    })
                }
            }
        }
    }
}