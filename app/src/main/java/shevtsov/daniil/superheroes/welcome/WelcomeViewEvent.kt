package shevtsov.daniil.superheroes.welcome

sealed class WelcomeViewEvent {
    object OpenCharacters : WelcomeViewEvent()
}
