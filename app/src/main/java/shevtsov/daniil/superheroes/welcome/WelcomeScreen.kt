package shevtsov.daniil.superheroes.welcome

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@Preview
@Composable
fun previewWelcome() {
    WelcomeContent(
        isButtonEnabled = false,
        message = "lol kek",
        onGoAction = { }
    )
}

@Composable
fun WelcomeScreen(navigateToCharactersAction: () -> Unit) {
    val viewModel: WelcomeViewModel = viewModel()
    val viewState by viewModel.state.collectAsState()
    val scope = rememberCoroutineScope()

    scope.launch {
        viewModel.events.collect { event ->
            when (event) {
                is WelcomeViewEvent.OpenCharacters -> {
                    navigateToCharactersAction.invoke()
                }
            }
        }
    }

    Surface(Modifier.fillMaxSize()) {
        WelcomeContent(
            message = viewState.message,
            isButtonEnabled = viewState.isButtonEnabled,
            onGoAction = viewModel::onGoAction
        )
    }
}


@Composable
fun WelcomeContent(
    message: String,
    isButtonEnabled: Boolean,
    onGoAction: () -> Unit
) {
    Column {
        Text(text = message)
        Button(
            onClick = onGoAction,
            enabled = isButtonEnabled
        ) {
            Text(text = "GO")
        }
    }

}