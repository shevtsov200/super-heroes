package shevtsov.daniil.superheroes.welcome

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.launch
import shevtsov.daniil.superheroes.core.util.toImmutable
import javax.inject.Inject

class WelcomeViewModel @Inject constructor(

) : ViewModel() {

    private val _state = MutableStateFlow(createInitState())
    val state = _state.toImmutable()

    private val _events = BroadcastChannel<WelcomeViewEvent>(1)
    val events = _events.asFlow()

    fun onGoAction() {
        viewModelScope.launch {
            _events.send(WelcomeViewEvent.OpenCharacters)
        }
    }

    private fun createInitState() = WelcomeViewState(
        message = "",
        isButtonEnabled = true
    )

}
