package shevtsov.daniil.superheroes.welcome

data class WelcomeViewState(
    val message: String,
    val isButtonEnabled: Boolean
)