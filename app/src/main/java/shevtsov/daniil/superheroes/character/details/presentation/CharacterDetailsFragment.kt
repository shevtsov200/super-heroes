package shevtsov.daniil.superheroes.character.details.presentation

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.palette.graphics.Palette
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import shevtsov.daniil.superheroes.R
import shevtsov.daniil.superheroes.character.details.presentation.errorhandling.CharacterDetailsErrorManager
import shevtsov.daniil.superheroes.character.details.presentation.model.CharacterDetailsViewState
import shevtsov.daniil.superheroes.core.SuperHeroesApplication
import shevtsov.daniil.superheroes.core.util.viewLifecycleLazy
import shevtsov.daniil.superheroes.databinding.FragmentCharacterDetailsBinding.bind
import javax.inject.Inject

class CharacterDetailsFragment : Fragment(R.layout.fragment_character_details) {

    private val binding by viewLifecycleLazy { bind(requireView()) }

    private val args: CharacterDetailsFragmentArgs by navArgs()

    @Inject
    lateinit var mainViewModelFactory: ViewModelProvider.Factory

    private val characterDetailsViewModel: CharacterDetailsViewModel by viewModels { mainViewModelFactory }

    @Inject
    lateinit var characterDetailsErrorManager: CharacterDetailsErrorManager


    override fun onAttach(context: Context) {
        super.onAttach(context)

        (context.applicationContext as SuperHeroesApplication)
            .getAppComponent()
            .inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val imageUrl = args.imageUrl

        setSharedElementTransitionOnEnter()
        postponeEnterTransition()

        binding.characterPortrait.apply {
            transitionName = imageUrl
            startEnterTransitionAfterLoadingImage(imageUrl, this)
        }

        initViews()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initObservers()
    }

    private fun initViews() {
        binding.changeCharacterButton.setOnClickListener { characterDetailsViewModel.showNewCharacter() }
    }

    private fun initObservers() {
//        mainViewModel.viewState.observe(viewLifecycleOwner, Observer { dataResult ->
//            renderLoading(isLoading = dataResult is DataResult.Loading)
//            when (dataResult) {
//                is DataResult.Success -> renderViewState(mainViewState = dataResult.value)
//                is DataResult.Error -> handleCharacterError(throwable = dataResult.throwable)
//            }
//        })
    }

    private fun renderLoading(isLoading: Boolean) {
        binding.changeCharacterButton.isEnabled = !isLoading
        binding.progressIndicator.isVisible = isLoading
    }

    private fun handleCharacterError(throwable: Throwable) {
        characterDetailsErrorManager.handleError(
            context = requireContext(),
            throwable = throwable,
            cancelAction = {},
            retryAction = { characterDetailsViewModel.showNewCharacter() }
        )
    }

    private fun renderViewState(characterDetailsViewState: CharacterDetailsViewState) {
        binding.characterName.text = characterDetailsViewState.name
//        loadCharacterPortrait(imageUrl = mainViewState.imageUrl)
        binding.copyright.text = characterDetailsViewState.copyright
    }

    private fun setSharedElementTransitionOnEnter() {
        sharedElementEnterTransition = TransitionInflater.from(context)
            .inflateTransition(R.transition.shared_element_transition)
    }

    private fun startEnterTransitionAfterLoadingImage(
        imageAddress: String,
        imageView: ImageView
    ) {
        Glide.with(this)
            .asBitmap()
            .load(imageAddress)
            .centerCrop()
            .dontAnimate()
            .listener(object : RequestListener<Bitmap> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Bitmap>?,
                    isFirstResource: Boolean
                ) = false

                override fun onResourceReady(
                    resource: Bitmap?,
                    model: Any?,
                    target: Target<Bitmap>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    resource?.let {
                        val palette = Palette.from(it).generate()

                        val backgroundColor = palette.getMutedColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.defaultBackgroundColor
                            )
                        )
                        val buttonColor = palette.getLightVibrantColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.defaultButtonColor
                            )
                        )
                        val buttonTextColor = palette.getDarkVibrantColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.defaultTextColor
                            )
                        )

                        binding.root.setBackgroundColor(backgroundColor)
                        binding.changeCharacterButton.setBackgroundColor(buttonColor)
                        binding.changeCharacterButton.setTextColor(buttonTextColor)
                    }

                    startPostponedEnterTransition()

                    return false
                }
            })
            .dontTransform()
            .onlyRetrieveFromCache(true)
            .into(imageView)
    }

}
