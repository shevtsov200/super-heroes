package shevtsov.daniil.superheroes.character.details.domain

import shevtsov.daniil.superheroes.character.details.data.CharacterEntity
import shevtsov.daniil.superheroes.character.details.domain.model.CharacterData
import javax.inject.Inject

class MapCharacterUseCase @Inject constructor(
    private val formImagePathUseCase: FormImagePathUseCase
) {

    operator fun invoke(character: CharacterEntity): CharacterData {
        val imageUrl = formImagePathUseCase(character.image)

        return CharacterData(
            characterId = character.id,
            name = character.name,
            imageUrl = imageUrl
        )
    }

}
