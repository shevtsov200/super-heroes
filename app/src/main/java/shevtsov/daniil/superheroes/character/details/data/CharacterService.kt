package shevtsov.daniil.superheroes.character.details.data

import retrofit2.http.GET
import retrofit2.http.Query
import shevtsov.daniil.superheroes.core.api.model.BaseResponse

interface CharacterService {

    @GET("characters")
    suspend fun getCharacters(
        @Query("offset") offset: Int?,
        @Query("limit") limit: Int?
    ): BaseResponse<CharacterEntity>

}
