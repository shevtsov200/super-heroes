package shevtsov.daniil.superheroes.character.details.domain

import kotlinx.coroutines.flow.Flow
import shevtsov.daniil.superheroes.character.details.domain.model.CharacterData
import shevtsov.daniil.superheroes.character.details.domain.model.GetOneCharacterUseCase
import javax.inject.Inject

class GetRandomCharacterUseCase @Inject constructor(
    private val getRandomCharacterNumberUseCase: GetRandomCharacterNumberUseCase,
    private val getOneCharacterUseCase: GetOneCharacterUseCase
) {


    suspend operator fun invoke(): Flow<CharacterData> {
        val characterNumber = getRandomCharacterNumberUseCase.invoke()

        return getOneCharacterUseCase.invoke(characterNumber)
    }

}
