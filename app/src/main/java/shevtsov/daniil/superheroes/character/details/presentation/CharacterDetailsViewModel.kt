package shevtsov.daniil.superheroes.character.details.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.zip
import kotlinx.coroutines.launch
import shevtsov.daniil.superheroes.character.details.domain.GetCopyrightUseCase
import shevtsov.daniil.superheroes.character.details.domain.GetRandomCharacterUseCase
import shevtsov.daniil.superheroes.character.details.domain.model.CharacterData
import shevtsov.daniil.superheroes.character.details.presentation.model.CharacterDetailsViewState
import shevtsov.daniil.superheroes.core.util.DataResult
import shevtsov.daniil.superheroes.core.util.toImmutable
import javax.inject.Inject

class CharacterDetailsViewModel @Inject constructor(
    private val getCopyrightUseCase: GetCopyrightUseCase,
    private val getRandomCharacterUseCase: GetRandomCharacterUseCase
) : ViewModel() {

    companion object {
        private val TAG = CharacterDetailsViewModel::class.java.simpleName
    }

    private val _viewState = MutableLiveData<DataResult<CharacterDetailsViewState>>()
    val viewState = _viewState.toImmutable()

    fun showNewCharacter() {
        viewModelScope.launch {
            _viewState.postValue(DataResult.Loading)

            val copyrightFlow = getCopyrightUseCase.invoke()
            val characterFlow = getRandomCharacterUseCase.invoke()

            copyrightFlow.zip(characterFlow) { copyright: String, characterData: CharacterData ->
                    CharacterDetailsViewState(
                        name = characterData.name,
                        imageUrl = characterData.imageUrl,
                        copyright = copyright
                    )
                }
                .catch { error ->
                    _viewState.postValue(DataResult.Error(error))
                }
                .collect { viewState ->
                    _viewState.postValue(DataResult.Success(viewState))
                }


        }

    }

}
