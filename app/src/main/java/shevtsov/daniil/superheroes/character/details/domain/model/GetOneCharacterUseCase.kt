package shevtsov.daniil.superheroes.character.details.domain.model

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import shevtsov.daniil.superheroes.character.details.domain.MapCharacterUseCase
import shevtsov.daniil.superheroes.character.list.domain.GetCharactersUseCase
import javax.inject.Inject

class GetOneCharacterUseCase @Inject constructor(
    private val getCharactersUseCase: GetCharactersUseCase,
    private val mapCharacterUseCase: MapCharacterUseCase
) {
    suspend operator fun invoke(characterNumber: Int): Flow<CharacterData> =
        getCharactersUseCase.invoke(
            offset = characterNumber,
            limit = 1
        ).map { characters ->
            val character = characters.first()

            mapCharacterUseCase.invoke(character)
        }

}
