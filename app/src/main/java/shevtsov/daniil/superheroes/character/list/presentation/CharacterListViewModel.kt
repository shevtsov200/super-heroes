package shevtsov.daniil.superheroes.character.list.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import shevtsov.daniil.superheroes.character.details.domain.model.CharacterData
import shevtsov.daniil.superheroes.character.list.domain.FormCharacterListUseCase
import shevtsov.daniil.superheroes.character.list.presentation.model.CharacterItem
import shevtsov.daniil.superheroes.character.list.presentation.model.CharacterListViewEvent
import shevtsov.daniil.superheroes.character.list.presentation.model.CharacterListViewState
import shevtsov.daniil.superheroes.core.util.toImmutable
import javax.inject.Inject

class CharacterListViewModel @Inject constructor(
    private val formCharacterListUseCase: FormCharacterListUseCase
) : ViewModel() {

    companion object {
        private const val MAX_CHARACTERS = 20
    }

    private val _state = MutableStateFlow(createInitialState())
    val state = _state.toImmutable()

    private val _events = BroadcastChannel<CharacterListViewEvent>(1)
    val events = _events.asFlow()

    init {
        loadCharacters()
    }

//    private val _viewState = MutableLiveData<DataResult<CharacterListViewState>>()
//    val viewState = _viewState.toImmutable()

//    private val _navigateToDetailsEvent = SingleLiveEvent<CharacterAdapter.Item>()
//    val navigateToDetailsEvent = _navigateToDetailsEvent.toImmutable()

    private fun loadCharacters() {
        viewModelScope.launch {
//            _viewState.postValue(DataResult.Loading)

            formCharacterListUseCase.invoke(MAX_CHARACTERS)
                .catch { error ->
//                    _viewState.postValue(DataResult.Error(error))
                }
                .collect { characters ->
                    val items = characters.map { it.toCharacterItem() }
                    val viewState = CharacterListViewState(items)

                    _state.emit(viewState)
//                    _viewState.postValue(DataResult.Success(viewState))
                }
        }
    }

    private fun CharacterData.toCharacterItem() = CharacterItem(
        id = characterId.toLong(),
        name = name,
        iconUrl = imageUrl
    )

    private fun createInitialState(): CharacterListViewState {
        return CharacterListViewState(
            characterList = emptyList()
        )
    }

    fun onCharacterSelected(iconUrl: String) {
        viewModelScope.launch {
            _events.send(CharacterListViewEvent.OpenDetails(iconUrl))
        }
    }

}
