package shevtsov.daniil.superheroes.character.list.presentation.model

sealed class CharacterListViewEvent {
    data class OpenDetails(val iconUrl: String) : CharacterListViewEvent()
}
