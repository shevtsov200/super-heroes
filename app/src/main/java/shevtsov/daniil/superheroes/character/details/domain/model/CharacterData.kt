package shevtsov.daniil.superheroes.character.details.domain.model

data class CharacterData(
    val characterId: Int,
    val name: String,
    val imageUrl: String
)
