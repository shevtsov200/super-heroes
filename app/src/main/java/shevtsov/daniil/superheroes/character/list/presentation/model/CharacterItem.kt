package shevtsov.daniil.superheroes.character.list.presentation.model

data class CharacterItem(
    val id: Long,
    val name: String,
    val iconUrl: String,
)