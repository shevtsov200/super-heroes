package shevtsov.daniil.superheroes.character.list.domain

import shevtsov.daniil.superheroes.character.details.data.CharacterRepository
import javax.inject.Inject

class GetCharactersUseCase @Inject constructor(
    private val characterRepository: CharacterRepository
) {

    suspend operator fun invoke(
        offset: Int,
        limit: Int
    ) = characterRepository.getCharacters(
        offset = offset,
        limit = limit
    )

}
