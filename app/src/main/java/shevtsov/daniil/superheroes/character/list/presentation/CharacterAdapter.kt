package shevtsov.daniil.superheroes.character.list.presentation

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import shevtsov.daniil.superheroes.core.GlideApp
import shevtsov.daniil.superheroes.databinding.ItemCharacterBinding

class CharacterAdapter(
    private val onItemClick: (item: Item, transitionView: View) -> Unit
) : ListAdapter<CharacterAdapter.Item, CharacterAdapter.ViewHolder>(CALLBACK) {

    companion object {
        private val CALLBACK = object : DiffUtil.ItemCallback<Item>() {
            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem.characterId == newItem.characterId
            }

            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCharacterBinding.inflate(inflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(holder.itemView.context, getItem(position), onItemClick)

    }

    class ViewHolder(
        private val binding: ItemCharacterBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            context: Context,
            item: Item,
            onItemClick: (item: Item, transitionView: View) -> Unit
        ) {
            binding.characterTitle.text = item.title
            binding.characterIcon.transitionName = item.iconUrl
            binding.root.setOnClickListener { onItemClick.invoke(item, binding.characterIcon) }

            GlideApp.with(context)
                .asBitmap()
                .load(item.iconUrl)
                .circleCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.characterIcon)
        }

    }

    data class Item(
        val characterId: Int,
        val title: String,
        val iconUrl: String
    )

}
