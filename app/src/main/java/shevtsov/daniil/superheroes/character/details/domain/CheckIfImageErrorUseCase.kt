package shevtsov.daniil.superheroes.character.details.domain

import javax.inject.Inject

class CheckIfImageErrorUseCase @Inject constructor() {

    companion object {
        private const val IMAGE_ERROR_STRING = "image_not_available"
    }

    operator fun invoke(imageUrl: String) = imageUrl.contains(IMAGE_ERROR_STRING)

}
