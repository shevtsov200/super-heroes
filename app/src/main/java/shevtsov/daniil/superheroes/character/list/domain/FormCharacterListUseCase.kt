package shevtsov.daniil.superheroes.character.list.domain

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import shevtsov.daniil.superheroes.character.details.domain.GetRandomCharacterNumberUseCase
import shevtsov.daniil.superheroes.character.details.domain.MapCharacterUseCase
import shevtsov.daniil.superheroes.character.details.domain.model.CharacterData
import javax.inject.Inject

class FormCharacterListUseCase @Inject constructor(
    private val getRandomCharacterNumberUseCase: GetRandomCharacterNumberUseCase,
    private val getCharactersUseCase: GetCharactersUseCase,
    private val mapCharacterUseCase: MapCharacterUseCase
) {

    suspend operator fun invoke(maxCharacters: Int): Flow<List<CharacterData>> {
        val characterOffset = getRandomCharacterNumberUseCase.invoke()
        return getCharactersUseCase.invoke(
            offset = characterOffset,
            limit = maxCharacters
        ).map { characters ->
            characters.map { characterEntity ->
                mapCharacterUseCase.invoke(characterEntity)
            }
        }
    }

}
