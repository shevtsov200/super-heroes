package shevtsov.daniil.superheroes.character.details.data

import com.google.gson.annotations.SerializedName
import shevtsov.daniil.superheroes.core.api.model.ImageEntity

data class CharacterEntity(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("thumbnail") val image: ImageEntity
)
