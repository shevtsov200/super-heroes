package shevtsov.daniil.superheroes.character.details.data

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import shevtsov.daniil.superheroes.core.api.model.BaseResponse
import javax.inject.Inject

class CharacterRepository @Inject constructor(
    private val characterService: CharacterService
) {

    suspend fun getCharacters(
        offset: Int? = null,
        limit: Int? = null
    ): Flow<List<CharacterEntity>> = flow {
        val character = characterService.getCharacters(
            offset = offset,
            limit = limit
        ).toEntity()

        emit(character)
    }

    private fun BaseResponse<CharacterEntity>.toEntity() = data.results

}
