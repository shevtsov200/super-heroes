package shevtsov.daniil.superheroes.character.details.domain

import javax.inject.Inject

class GetRandomCharacterNumberUseCase @Inject constructor() {
    companion object {
        //TODO: Use case should not be aware of this
        private const val MAX_CHARACTERS_COUNT = 1490
    }

    operator fun invoke() = (0 until MAX_CHARACTERS_COUNT).random()

}
