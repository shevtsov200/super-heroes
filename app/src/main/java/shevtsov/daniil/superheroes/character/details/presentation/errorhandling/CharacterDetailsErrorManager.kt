package shevtsov.daniil.superheroes.character.details.presentation.errorhandling

import android.content.Context
import shevtsov.daniil.superheroes.core.errorhandling.BaseErrorManager
import shevtsov.daniil.superheroes.core.errorhandling.CommonDialogManager
import javax.inject.Inject

class CharacterDetailsErrorManager @Inject constructor(
    private val commonDialogManager: CommonDialogManager
) : BaseErrorManager(commonDialogManager) {

    //TODO: Differentiate between different error codes
    fun handleError(
        context: Context,
        throwable: Throwable,
        cancelAction: () -> Unit,
        retryAction: () -> Unit
    ) {
        commonDialogManager.showRetryDialog(
            context = context,
            cancelAction = cancelAction,
            retryAction = retryAction
        )
    }

}
