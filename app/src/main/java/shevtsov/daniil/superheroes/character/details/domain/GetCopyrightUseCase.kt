package shevtsov.daniil.superheroes.character.details.domain

import shevtsov.daniil.superheroes.character.details.data.CopyrightRepository
import javax.inject.Inject

class GetCopyrightUseCase @Inject constructor(
    private val copyrightRepository: CopyrightRepository
) {

    operator fun invoke() = copyrightRepository.getCopyright()

}
