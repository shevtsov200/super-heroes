package shevtsov.daniil.superheroes.character.list.presentation

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.compose.material.MaterialTheme
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import shevtsov.daniil.superheroes.R
import shevtsov.daniil.superheroes.core.SuperHeroesApplication
import shevtsov.daniil.superheroes.core.util.viewLifecycleLazy
import shevtsov.daniil.superheroes.databinding.FragmentCharacterListBinding
import javax.inject.Inject

class CharacterListFragment : Fragment(R.layout.fragment_character_list) {

    private val binding by viewLifecycleLazy {
        FragmentCharacterListBinding.bind(requireView())
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: CharacterListViewModel by viewModels { viewModelFactory }

//    private var characterAdapter: CharacterAdapter? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)

        (context.applicationContext as SuperHeroesApplication)
            .getAppComponent()
            .inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            composeView.setContent {
                MaterialTheme {
                    CharacterListScreen(
                        viewModel = viewModel,
                        toDetailsAction = ::navigateToDetails
                    )
                }
            }
        }
    }

    //    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//        viewModel.loadCharacters()
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//
//        initViews()
//    }
//
//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//
//        initObservers()
//    }
//
//    override fun onDestroyView() {
//        characterAdapter = null
//
//        super.onDestroyView()
//    }
//
//    private fun initViews() {
//        postponeEnterTransition()
//        view?.viewTreeObserver?.addOnPreDrawListener {
//            startPostponedEnterTransition()
//
//            true
//        }
//
//        characterAdapter = CharacterAdapter(
//            onItemClick = { item, transitionView ->
//                navigateToDetails(item, transitionView)  //TODO: Find a way to use viewModel
//            }
//        )
//
//        binding.charactersRecyclerView.adapter = characterAdapter
//        binding.charactersRecyclerView.layoutManager = LinearLayoutManager(context)
//    }
//
//    private fun initObservers() {
//        viewModel.viewState.observe(viewLifecycleOwner, Observer { dataResult ->
//            renderLoading(isLoading = dataResult is DataResult.Loading)
//
//            when (dataResult) {
//                is DataResult.Success -> characterAdapter?.submitList(dataResult.value.characterList)
//            }
//        })
//    }
//
//    private fun renderLoading(isLoading: Boolean) {
//        binding.charactersRecyclerView.isVisible = !isLoading
//        binding.charactersProgressIndicator.isVisible = isLoading
//    }
//
    private fun navigateToDetails(iconUrl: String/*, transitionView: View*/) {
        val extras = FragmentNavigatorExtras(
//            transitionView to item.iconUrl
        )

        val directions = CharacterListFragmentDirections.navigateToDetails(
            imageUrl = iconUrl
        )

        findNavController().apply {
            if (currentDestination?.id == R.id.character_list_fragment) {
                navigate(directions, extras)
            }
        }
    }

}
