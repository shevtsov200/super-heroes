package shevtsov.daniil.superheroes.character.list.presentation

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Arrangement.spacedBy
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bumptech.glide.request.RequestOptions
import dev.chrisbanes.accompanist.glide.GlideImage
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import shevtsov.daniil.superheroes.R
import shevtsov.daniil.superheroes.character.list.presentation.model.CharacterItem
import shevtsov.daniil.superheroes.character.list.presentation.model.CharacterListViewEvent


@Preview(
    showBackground = true,
    backgroundColor = 0xFFFFFFFF
)
@Composable
fun previewCharacterList() {
    CharacterListContent(
        characters = listOf(
            CharacterItem(
                id = 0L,
                name = "Cyclops",
                iconUrl = "http://i.annihil.us/u/prod/marvel/i/mg/6/70/526547e2d90ad/portrait_incredible.jpg",
            ),
            CharacterItem(
                id = 1L,
                name = "Bob",
                iconUrl = "http://i.annihil.us/u/prod/marvel/i/mg/6/70/526547e2d90ad/portrait_incredible.jpg"
            )
        ),
        onItemClick = {}
    )
}

@Composable
fun CharacterListScreen(
    viewModel: CharacterListViewModel,
    toDetailsAction: (iconUrl: String) -> Unit
) {
    val viewState by viewModel.state.collectAsState()
    val scope = rememberCoroutineScope()

    scope.launch {
        viewModel.events.collect { event ->
            when (event) {
                is CharacterListViewEvent.OpenDetails -> {
                    toDetailsAction.invoke(event.iconUrl)
                }
            }
        }
    }

    Surface(Modifier.fillMaxSize()) {
        CharacterListContent(
            characters = viewState.characterList,
            onItemClick = { item -> viewModel.onCharacterSelected(iconUrl = item.iconUrl) }
        )
    }
}

@Composable
fun CharacterListContent(
    characters: List<CharacterItem>,
    onItemClick: (item: CharacterItem) -> Unit
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .background(colorResource(id = R.color.characterListBackground))
            .padding(16.dp),
        verticalArrangement = spacedBy(12.dp)

    ) {
        items(characters) { character ->
            CharacterRow(
                name = character.name,
                imageUrl = character.iconUrl,
                onClick = { onItemClick(character) }
            )
        }
    }
}

@Composable
fun CharacterRow(
    imageUrl: String,
    name: String,
    onClick: () -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .clickable(
                onClick = onClick
            )
            .background(colorResource(R.color.characterItemBackground))
    ) {
        GlideImage(
            modifier = Modifier
                .size(85.dp)
                .padding(vertical = 16.dp)
                .padding(start = 16.dp),
            data = imageUrl,
            contentDescription = "",
            requestBuilder = {
                val options = RequestOptions().apply {
                    circleCrop()
                }
                apply(options)
            }
        )
        Text(
            modifier = Modifier.padding(16.dp),
            text = name,
            fontSize = 35.sp,
            color = Color.Black,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
    }
}