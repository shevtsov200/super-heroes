package shevtsov.daniil.superheroes.character.details.presentation.model

data class CharacterDetailsViewState(
    val name: String,
    val imageUrl: String,
    val copyright: String
)
