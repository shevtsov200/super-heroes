package shevtsov.daniil.superheroes.character.list.presentation.model

data class CharacterListViewState(
    val characterList: List<CharacterItem>
)
