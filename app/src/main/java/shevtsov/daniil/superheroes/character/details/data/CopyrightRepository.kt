package shevtsov.daniil.superheroes.character.details.data

import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class CopyrightRepository @Inject constructor(
    private val characterService: CharacterService
) {

    //TODO: Come up with something better
    fun getCopyright() = flow {
        val copyright = characterService.getCharacters(
            offset = 0,
            limit = 1
        ).copyright
        emit(copyright)
    }
}
