package shevtsov.daniil.superheroes.character.details.domain

import shevtsov.daniil.superheroes.core.api.model.ImageEntity
import javax.inject.Inject

class FormImagePathUseCase @Inject constructor() {

    companion object {
        private const val DEFAULT_IMAGE_NAME = "portrait_incredible"
    }

    operator fun invoke(imageEntity: ImageEntity) =
        "${imageEntity.path}/$DEFAULT_IMAGE_NAME.${imageEntity.extension}"

}
