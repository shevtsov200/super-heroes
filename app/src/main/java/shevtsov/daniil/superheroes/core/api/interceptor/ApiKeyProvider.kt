package shevtsov.daniil.superheroes.core.api.interceptor

import shevtsov.daniil.superheroes.BuildConfig
import javax.inject.Inject

class ApiKeyProvider @Inject constructor() {

    fun getPrivateKey() = BuildConfig.PRIVATE_KEY

    fun getPublicKey() = BuildConfig.PUBLIC_KEY

}
