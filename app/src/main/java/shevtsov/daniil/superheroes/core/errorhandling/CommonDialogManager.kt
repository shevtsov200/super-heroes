package shevtsov.daniil.superheroes.core.errorhandling

import android.content.Context
import shevtsov.daniil.superheroes.R
import javax.inject.Inject

class CommonDialogManager @Inject constructor() : BaseDialogManager() {

    fun showRetryDialog(
        context: Context,
        cancelAction: () -> Unit,
        retryAction: () -> Unit
    ) = showSimpleDialog(
        context = context,
        titleId = R.string.error_dialog_title,
        descriptionId = R.string.error_dialog_description,
        leftButtonTitleId = R.string.common_cancel,
        leftButtonAction = cancelAction,
        rightButtonTitleId = R.string.common_retry,
        rightButtonAction = retryAction
    )

}
