package shevtsov.daniil.superheroes.core

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import shevtsov.daniil.superheroes.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
