package shevtsov.daniil.superheroes.core.api.interceptor

data class QueryParameter(
    val name: String,
    val value: String
)
