package shevtsov.daniil.superheroes.core.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

fun <T> MutableLiveData<T>.toImmutable() = this as LiveData<T>

fun <T> MutableStateFlow<T>.toImmutable() = this as StateFlow<T>
