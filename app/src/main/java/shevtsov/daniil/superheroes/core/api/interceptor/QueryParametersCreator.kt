package shevtsov.daniil.superheroes.core.api.interceptor

import javax.inject.Inject

class QueryParametersCreator @Inject constructor(
    private val keyProvider: ApiKeyProvider,
    private val timestampProvider: TimestampProvider,
    private val hashCreator: HashCreator
) {

    fun createQueryParameters(): List<QueryParameter> {
        val timestamp = timestampProvider.getTimestamp()
        val privateKey = keyProvider.getPrivateKey()
        val publicKey = keyProvider.getPublicKey()

        val stringToHash = "$timestamp$privateKey$publicKey"
        val hash = hashCreator.calculateHash(stringToHash)

        return listOf(
            QueryParameter(
                name = "ts",
                value = timestamp.toString()
            ),
            QueryParameter(
                name = "apikey",
                value = publicKey
            ),
            QueryParameter(
                name = "hash",
                value = hash
            )
        )

    }

}
