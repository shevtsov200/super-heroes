package shevtsov.daniil.superheroes.core.api.model

import com.google.gson.annotations.SerializedName

data class ImageEntity(
    @SerializedName("path") val path: String,
    @SerializedName("extension") val extension: String
)