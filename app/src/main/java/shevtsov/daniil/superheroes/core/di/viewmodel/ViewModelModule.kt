package shevtsov.daniil.superheroes.core.di.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import shevtsov.daniil.superheroes.character.details.presentation.CharacterDetailsViewModel
import shevtsov.daniil.superheroes.character.list.presentation.CharacterListViewModel
import shevtsov.daniil.superheroes.core.di.AppScope

@Module
interface ViewModelModule {
    @Binds
    @AppScope
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(CharacterDetailsViewModel::class)
    fun bindMainViewModel(viewModel: CharacterDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CharacterListViewModel::class)
    fun bindCharacterListViewModel(viewModel: CharacterListViewModel): ViewModel

}
