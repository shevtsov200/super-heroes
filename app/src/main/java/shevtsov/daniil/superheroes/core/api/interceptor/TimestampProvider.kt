package shevtsov.daniil.superheroes.core.api.interceptor

import java.util.*
import javax.inject.Inject

class TimestampProvider @Inject constructor() {

    fun getTimestamp() = Calendar.getInstance().timeInMillis

}
