package shevtsov.daniil.superheroes.core.api.interceptor

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class QueryParametersInterceptor @Inject constructor(
    private val queryParametersCreator: QueryParametersCreator
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val url = request.url
            .newBuilder()
            .appendQueryParameters()
            .build()

        return chain.proceed(request.newBuilder().url(url).build())
    }

    private fun HttpUrl.Builder.appendQueryParameters() = apply {
        queryParametersCreator.createQueryParameters()
            .forEach { queryParameter ->
                addQueryParameter(
                    queryParameter.name,
                    queryParameter.value
                )
            }
    }
}