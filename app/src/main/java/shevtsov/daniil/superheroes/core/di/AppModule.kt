package shevtsov.daniil.superheroes.core.di

import dagger.Module
import shevtsov.daniil.superheroes.core.di.network.NetworkModule
import shevtsov.daniil.superheroes.core.di.viewmodel.ViewModelModule

@Module(
    includes = [
        ViewModelModule::class,
        NetworkModule::class
    ]
)
class AppModule
