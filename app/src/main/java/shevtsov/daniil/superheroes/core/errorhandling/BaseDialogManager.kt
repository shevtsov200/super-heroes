package shevtsov.daniil.superheroes.core.errorhandling

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.core.view.isGone
import shevtsov.daniil.superheroes.databinding.DialogCommonBinding

abstract class BaseDialogManager {

    protected fun showSimpleDialog(
        context: Context,
        @StringRes titleId: Int? = null,
        @StringRes descriptionId: Int? = null,
        @StringRes leftButtonTitleId: Int? = null,
        leftButtonAction: () -> Unit = {},
        @StringRes rightButtonTitleId: Int? = null,
        rightButtonAction: () -> Unit = {}
    ) = showSimpleDialog(
        context = context,
        title = context.getNullableString(titleId),
        description = context.getNullableString(descriptionId),
        leftButtonTitle = context.getNullableString(leftButtonTitleId),
        leftButtonAction = leftButtonAction,
        rightButtonTitle = context.getNullableString(rightButtonTitleId),
        rightButtonAction = rightButtonAction
    )

    protected fun showSimpleDialog(
        context: Context,
        title: String? = null,
        description: String? = null,
        leftButtonTitle: String? = null,
        leftButtonAction: () -> Unit = {},
        rightButtonTitle: String? = null,
        rightButtonAction: () -> Unit = {}
    ) {
        var binding: DialogCommonBinding?
        AlertDialog.Builder(context)
            .apply {
                val inflater = LayoutInflater.from(context)
                binding = DialogCommonBinding.inflate(inflater)
                val view = binding?.root
                setView(view)
            }
            .setCancelable(false)
            .setOnDismissListener { binding = null }
            .show()
            .also { dialog ->
                binding?.let { binding ->
                    binding.title.setTextOrHide(title)
                    binding.description.setTextOrHide(description)

                    binding.leftButton.apply {
                        setTextOrHide(leftButtonTitle)
                        setOnClickListener {
                            leftButtonAction.invoke()

                            dialog.dismiss()
                        }
                    }

                    binding.rightButton.apply {
                        setTextOrHide(rightButtonTitle)
                        setOnClickListener {
                            rightButtonAction.invoke()

                            dialog.dismiss()
                        }
                    }
                }
            }
    }

    private fun Context.getNullableString(@StringRes stringId: Int?) =
        stringId?.let(this::getString)

    private fun TextView.setTextOrHide(string: String?) {
        if (string != null) {
            text = string
        } else {
            isGone = true
        }
    }

    private fun Button.setTextOrHide(string: String?) {
        if (string != null) {
            text = string
        } else {
            isGone = true
        }
    }
}
