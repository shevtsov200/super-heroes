package shevtsov.daniil.superheroes.core.api.interceptor

import java.security.MessageDigest
import javax.inject.Inject

class HashCreator @Inject constructor() {

    fun calculateHash(stringToHash: String) = MessageDigest.getInstance("MD5")
        .digest(stringToHash.toByteArray())
        .joinToString("") { it.toHex() }

    private fun Byte.toHex() = String.format("%02x", this)

}
