package shevtsov.daniil.superheroes.core

import android.app.Application
import shevtsov.daniil.superheroes.core.di.AppComponent
import shevtsov.daniil.superheroes.core.di.DaggerAppComponent

class SuperHeroesApplication : Application() {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.create()
    }

    fun getAppComponent() = appComponent

}
