package shevtsov.daniil.superheroes.core.util

sealed class DataResult<out T> {

    object Loading : DataResult<Nothing>()

    data class Success<T>(val value: T) : DataResult<T>()

    data class Error(val throwable: Throwable) : DataResult<Nothing>()

}
