package shevtsov.daniil.superheroes.core.di

import dagger.Component
import shevtsov.daniil.superheroes.character.details.presentation.CharacterDetailsFragment
import shevtsov.daniil.superheroes.character.list.presentation.CharacterListFragment

@AppScope
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(characterDetailsFragment: CharacterDetailsFragment)

    fun inject(characterListFragment: CharacterListFragment)

}
